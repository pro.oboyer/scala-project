package Manager

import Models._

import scala.collection.Iterator
import scala.io.Source
import scala.util.{Either, Failure, Success, Try}

/**
  * A singleton which permit to read a file and his line to initiate the playground and his mowers on it.
  */
object FileManager {

  /**
    * Create a list of String from a file.
    * @param filePath path to file which we take the lines.
    * @return a Try object which contains a list of string.
    */
  def readFileToList(filePath: String): Try[List[String]] = Try(Source.fromFile(filePath).getLines().toList)

  /**
    * read a string to create a position which match to the right top corner of the playground and it will define the size of playground
    * @param line line which will read in order to retrieve the size of playground.
    * @return a Try object which contains a Position.
    */
  def readSizePlayGround(line: String): Try[Position] = {
    val words = line.trim.split(" ").toList
    val w1 = words.head
    val w2 = words.tail.head

    Try(new Position(w1.toInt,w2.toInt));
  }

  /**
    * read a file to create a list of Command which give us the information to move a mower
    * @param line it is a string which contains command
    * @return lis of Command
    */
  def readMowerCommand(line:String):List[Command] = {
    Command.initCommand(line.trim.split("").toList)
  }


  /**
    * Read a position and direction of a mower in a line of String.
    * @param line a String which contains informations.
    * @return a Either object which contains in his left a boolean to indicate no valid cardinal position
    *         and in his right a Try object which contains a start position of a mower.
    *
    */
  def readPositionAndDirectionMower(line:String):Either[Boolean,Try[(Position,CardinalPoints)]] =

      line.trim.split(" ").toList match {
      case x :: y :: z :: tail =>

        val cp = CardinalPoints.init(z)
        cp match {
          case None => Left(false)
          case Some(cardinal) => Right(Try(new Position(x.toInt, y.toInt), cardinal))
        }
      case _ => Left(false)

      }

  /**
    * delete empty lines in a list of string.
    * @param lines list of string which can contains empty lines.
    * @return a list of String with no empty lines.
    */
  def deleteEmptyLine(lines:List[String]):List[String]= lines match{
    case h::t => {
      h match {
        case line if(line.trim.size == 0) => deleteEmptyLine(t)
        case line => line.trim().replaceAll("\n","")::deleteEmptyLine(t)
      }

    }
    case Nil => Nil

  }

  /**
    * create a list of Mower from a list of String.
    * @param lines Contains information to create mowers.
    * @return a list of Mower or an empty List.
    */
  def createListMower(lines: List[String],sizePlayGround: Position):List[Mower] = lines match{
    case Nil => Nil
    case pos::com::t =>
      val position = readPositionAndDirectionMower(pos)
      val command = readMowerCommand(com)

          position match {
          case Left(p) => createListMower(t,sizePlayGround)
          case Right(p) => p match{
            case Success(n) =>
              val pos = (n._2,n._1)
              pos match {
                case (a,b) if(b.x >= 0 && b.y >= 0 && b.x <= sizePlayGround.x && b.y <= sizePlayGround.y) => new Mower(n._2,n._1,command)::createListMower(t,sizePlayGround)
                case _ => createListMower(t,sizePlayGround)
              }
            case Failure(exception) => println("toInt ne marche pas pour les chaines de caractères qui ne sont pas des nombres : " + exception.getLocalizedMessage)
              createListMower(t,sizePlayGround)
          }
          case _ => createListMower(t,sizePlayGround)

      }
    case _ => Nil
  }

  /**
    * print all line in the list
    * @param list conatins a list of string
    * @return true when list is traveled
    */
  def printList(list: List[String]):Boolean= list match {
    case h::tail => println(h)
      printList(tail)
    case Nil => true
  }

}
