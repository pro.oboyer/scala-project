package Launcher

import Manager.FileManager
import Models.{PlayGround, Position}

import scala.util.{Success, Try, Failure}

object Main extends App {

 val fileManager = FileManager
 val tryLines = fileManager.readFileToList("src/file.txt")
 tryLines match {
   case Success(lines) => {
    val treatLines = FileManager.deleteEmptyLine(lines)
    val sizePlayGround = FileManager.readSizePlayGround(treatLines.head)
    sizePlayGround match {
      case Success(sizeP) => val listMower = FileManager.createListMower(treatLines.tail,sizeP)
       val playGround = Try(new PlayGround(listMower,sizeP))
        playGround match {
          case Success(playGround) =>
            val travels = playGround.mowerTravel()
            fileManager.printList(travels)
          case Failure(exception) => print("Les valeurs entrées pour la taille du terrain sont interdites car inférieur à 1")
        }

      case Failure(exception) =>
       print("Les valeurs entrées sur la première ligne du fichier ne sont pas des nombres/chiffres : " + exception.getLocalizedMessage)
    }
   }
   case Failure(exception) =>
    print("Echec lors de l'ouverture du fichier, vérifier que celui ci existe ou que vous ayez les droits nécessaire : " + exception.getLocalizedMessage)
  }



}
