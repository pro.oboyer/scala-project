package Models

/**
  * Represents cardinal points
  */
sealed trait CardinalPoints

/**
  * Singleton which create a CardinalPoint
  */
object CardinalPoints {
  /**
    * From a string which is a letter, we create a CardinalPoints.
    * if letter doesn't match to 'N', 'S', 'E', or 'W' it ignore this letter
    * else create the cardinalPoints associate to the letter
    * @param cardinalP string contains the letter which determine the CardinalPoints
    * @return Option object which contains a CardinalPoints or None value
    */
  def init(cardinalP:String):Option[CardinalPoints] = cardinalP match{

    case "N" => Some(CardinalPoints.North)
    case "S" => Some(CardinalPoints.South)
    case "E" => Some(CardinalPoints.East)
    case "W" => Some(CardinalPoints.West)
    case all => println("Le caractère " + all + "ne correspond pas à un point cardinal")
      None

  }

  case object North extends CardinalPoints
  case object South extends CardinalPoints
  case object East extends CardinalPoints
  case object West extends CardinalPoints

}
