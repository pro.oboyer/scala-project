package Models


/**
  * Class which represent a Position on the playground
  * @param x absciss of the playground
  * @param y ordonate of the playground
  */
case class Position( val x: Int,  val y: Int){


}