package Models

import Models.CardinalPoints.{North, South, West, East}

/**
  * A mower which have a start position, a direction, a list of command to move on a playground.
  * @param direction define the beginning direction.
  * @param startPosition define start position on playground.
  * @param listCommand define a list of command to move mower on a playground.
  */
case class Mower(private val direction: CardinalPoints, private val startPosition:Position, private val listCommand: List[Command]  ){

  /**
    *
    * @return direction of the mower.
    */
  def getCardinalPoints():CardinalPoints = direction

  /**
    *
    * @return start position of the mower.
    */
  def getPosition(): Position = startPosition

  /**
    * Describe a journey of this mower on a playground.
    * @param sizePlayground size of the playground permits to move on this playground.
    * @return a string which contains a journey of a mower.
    */
  def journey(sizePlayground:Position):String = startPosition match {
    case Position(-1,_) => ""
    case Position(_,-1) => ""
    case Position(_,_) =>
      val finish = mowerFinishPosition(sizePlayground,startPosition,direction,listCommand)
      finish match {
        case Some(finish) =>  "La direction est "+direction+" positionné à la position "+startPosition + " position de fin " + finish
        case None => "La direction est "+direction+" positionné à la position "+startPosition + " position de fin "
      }
    case _ => ""
  }

  /**
    * Move a mower in a Playground and permit to know his last position on the playground.
    * @param sizePlayground Describe the size of a playground.
    * @param startPosition position of a mower
    * @param direction direction of a mower
    * @param list list of command to move a mower
    * @return Option object with a (Position, CardinalPoints, list of Command)
    */
  private def mowerFinishPosition(sizePlayground: Position,startPosition: Position, direction: CardinalPoints, list: List[Command]):Option[(Position,CardinalPoints,List[Command])]= list match {
    case l::tail =>
      l match {
        case D =>  direction match {
            case North => mowerFinishPosition(sizePlayground,startPosition,East,tail)
            case East => mowerFinishPosition(sizePlayground,startPosition,South,tail)
            case South => mowerFinishPosition(sizePlayground,startPosition,West,tail)
            case West => mowerFinishPosition(sizePlayground,startPosition,North,tail)
        }
        case G =>  direction match {
          case North => mowerFinishPosition(sizePlayground,startPosition,West,tail)
          case West => mowerFinishPosition(sizePlayground,startPosition,South,tail)
          case South => mowerFinishPosition(sizePlayground,startPosition,East,tail)
          case East => mowerFinishPosition(sizePlayground,startPosition,North,tail)
        }
        case A =>  direction match {
          case North => startPosition match {
            case Position(a, b) if(a < 0 || b < 0 || b+1 > sizePlayground.y || a > sizePlayground.x) =>

              mowerFinishPosition(sizePlayground,startPosition,direction,tail)
            case Position(a,b) =>  mowerFinishPosition(sizePlayground,new Position(startPosition.x , startPosition.y+1),direction,tail)
            case _ => mowerFinishPosition(sizePlayground, startPosition,direction,tail)
          }
          case East =>startPosition match {
            case Position(a, b) if(a < 0 || b < 0 || b > sizePlayground.y || a+1 > sizePlayground.x) =>

              mowerFinishPosition(sizePlayground,startPosition,direction,tail)
            case Position(a,b) =>  mowerFinishPosition(sizePlayground,new Position(startPosition.x+1 , startPosition.y),direction,tail)
            case _ => mowerFinishPosition(sizePlayground, startPosition,direction,tail)
          }
          case South => startPosition match {
            case Position(a, b) if (a < 0 || b-1 < 0 || b > sizePlayground.y || a > sizePlayground.x) =>

              mowerFinishPosition(sizePlayground, startPosition, direction, tail)
            case Position(a,b) => mowerFinishPosition(sizePlayground, new Position(startPosition.x, startPosition.y - 1), direction, tail)
            case _ => mowerFinishPosition(sizePlayground, startPosition,direction,tail)
          }
          case West =>  startPosition match {
            case Position(a, b) if (a-1 < 0 || b < 0 || b > sizePlayground.y || a > sizePlayground.x) =>
              mowerFinishPosition(sizePlayground, startPosition,direction,tail)
            case Position(a,b) =>  mowerFinishPosition(sizePlayground,new Position(startPosition.x-1 , startPosition.y),direction,tail)
            case _ => mowerFinishPosition(sizePlayground, startPosition,direction,tail)
          }
        }

        case _ => mowerFinishPosition(sizePlayground,startPosition,direction,tail)
      }
    case Nil => Some((startPosition, direction, Nil))
  }
}
