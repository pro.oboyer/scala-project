package Models

/**
  * D match to Right
  * G match to Left
  * A match to Forward
  */
sealed trait Command
case object D extends Command
case object G extends Command
case object A extends Command

/**
  * Singleton to create a list of Command
  */
object Command{

  /**
    * Read a list of String which contains a letter and permits to create a list of Command.
    * if the letter no corresponding to 'A', 'D', 'G' it ignore else it create the valid command associate to it.
    * @param list contains a list of letter
    * @return a list of command or empty list.
    */
  def initCommand(list:List[String]): List[Command] = list match{
    case l::tail => l match {
      case "D" => D::initCommand(tail)
      case "G" => G::initCommand(tail)
      case "A" => A::initCommand(tail)
      case _ => initCommand(tail)
    }
    case Nil => Nil
  }
}
