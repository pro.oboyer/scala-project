package Models

/**
  * Playground is a table where there are mowers.
  * @param mowers list of mower in this playground
  * @param sizePlayground it is the top right corner of a playground and permits to know the position
  */
case class PlayGround(private val mowers:List[Mower],private val sizePlayground:Position){
require(sizePlayground.x > 0 && sizePlayground.y > 0)
  /**
    * move the mowers on this playground and describes their travel
    * @return a list of String which describes the journey of all mowers
    */
  def mowerTravel()= mowers.map(m => m.journey(sizePlayground))


}

