package Models

import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Try}

class PlayGroundTest extends FlatSpec with Matchers {
  "PlayGround" should "compute all test on PlayGround" in {
    PlayGround(List(Mower(CardinalPoints.North,Position(1,2),Command.initCommand("GAGAGAGAA".split("").toList))),Position(7,7)).mowerTravel() should be (List("La direction est North positionné à la position Position(1,2) position de fin (Position(1,3),North,List())"))
  }

}