package Models

import Models.CardinalPoints.North
import org.scalatest.{FlatSpec, Matchers}

class CommandTest extends FlatSpec with Matchers   {

  "Command" should "compute all test on Command" in {

    Command.initCommand(List("A","B","A","G","G","D")) should be (List(A,A,G,G,D))

  }

}
