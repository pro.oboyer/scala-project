package Models

import Models.CardinalPoints.North
import org.scalatest.{FlatSpec, Matchers}

class MowerTest extends FlatSpec with Matchers   {

  "Mower" should "compute all test on Mower" in {

    Mower(CardinalPoints.North,Position(1, 1), Command.initCommand("GGAGGADD".split("").toList)) should be(Mower(North, Position(1, 1), List(G, G, A, G, G, A, D, D)))
    Mower(CardinalPoints.North,Position(1,1),Command.initCommand("GGFFAGGADD".split("").toList))should be(Mower(North,Position(1,1),List(G,G,A,G,G,A,D,D)))
    Mower(CardinalPoints.North,Position(1,1),Command.initCommand("".split("").toList))should be(Mower(North,Position(1,1),Nil))
    Mower(CardinalPoints.North,Position(1,1),Command.initCommand("FFFF".split("").toList))should be(Mower(North,Position(1,1),Nil))
    Mower(CardinalPoints.North,Position(1,2),Command.initCommand("GAGAGAGAA".split("").toList)).journey(Position(7,7)) should be("La direction est North positionné à la position Position(1,2) position de fin (Position(1,3),North,List())")
  }

}
