package Models

import org.scalatest.{FlatSpec, Matchers}

class CardinalPointsTest extends FlatSpec with Matchers   {

  "CardinalPoints" should "compute all test on CardinalPoints" in {

    CardinalPoints.init("N") should be (Some(CardinalPoints.North))
    CardinalPoints.init("S") should be (Some(CardinalPoints.South))
    CardinalPoints.init("E") should be (Some(CardinalPoints.East))
    CardinalPoints.init("W") should be (Some(CardinalPoints.West))
    CardinalPoints.init("1") should be (None)
    CardinalPoints.init("K") should be (None)

  }

}
