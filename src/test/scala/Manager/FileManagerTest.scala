package Manager
import Models.CardinalPoints.{North, South}
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Either, Failure, Success, Try}
import Models._
class FileManagerTest extends FlatSpec with Matchers   {

  "FileManager" should "compute all test on FileManager" in {
    val text = FileManager.readFileToList("src/fileTest.txt") should be(Success(List("7 9", "1 2 N", "GAGAGAGAA", "")))
    val l = FileManager.readFileToList("src/fileNotExist.txt").isFailure should be (true)
    FileManager.readSizePlayGround("7 7") should be(Success(Position(7,7)))
    FileManager.readSizePlayGround("A FF z").isFailure should be (true)
    FileManager.readSizePlayGround("a 7").isFailure should be (true)
    FileManager.readSizePlayGround("7 b").isFailure should be (true)
    FileManager.readMowerCommand("ZZZAAAAFFFAAAADDDZZZGGGZZZ") should be(List(A,A,A,A,A,A,A,A,D,D,D,G,G,G))
    FileManager.readMowerCommand("ZZZZZZ") should be(Nil)
    FileManager.readPositionAndDirectionMower("1 2 N") should be (Right(Try(new Position(1, 2), North)))
    FileManager.readPositionAndDirectionMower("1 2 D") should be (Left(false))
    FileManager.readPositionAndDirectionMower("1") should be (Left(false))
    FileManager.readPositionAndDirectionMower("1 2") should be (Left(false))
    FileManager.deleteEmptyLine(List("truc","","   ","  ","hahaha")) should be(List("truc","hahaha"))
    FileManager.deleteEmptyLine(List("","","   ","  ","")) should be(Nil)
    FileManager.createListMower(List(),Position(7,7)) should be(Nil)
    FileManager.createListMower(List("Must","Send","Nil"),Position(7,7)) should be(Nil)
    FileManager.createListMower(List("1 2 N","AAGGAA","1 5 S","AADAGAA"),Position(7,7)) should be(List(Mower(North,Position(1,2),List(A,A,G,G,A,A)),Mower(South,Position(1,5),List(A,A,D,A,G,A,A))))
    FileManager.printList(List("Test","afficher")) should be(true)
  }
}
