name := "oboyer-mower"

version := "0.1"

scalaVersion := "2.12.7"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
libraryDependencies += "eu.timepit" %% "refined" % "0.9.2"